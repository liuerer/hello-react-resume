import React from 'react';
import HeadImage from '../assets/avatar.jpg';
import '../styles/Header.less';

class Header extends React.Component {
  render() {
    const { name, age } = this.props;
    return (
      <header>
        <img
          className="image-size"
          src={HeadImage}
          alt="Image does not exist"
        />
        <h1>Hello,</h1>
        <h2>
          My name is {name} {age}YO and this is my resume/cv
        </h2>
      </header>
    );
  }
}

export default Header;
