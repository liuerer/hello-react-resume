import React from 'react';

class Description extends React.Component {
  render() {
    const { description } = this.props;
    return (
      <section className="about-me">
        <h3>ABOUT ME</h3>
        <p>{description}</p>
      </section>
    );
  }
}

export default Description;
