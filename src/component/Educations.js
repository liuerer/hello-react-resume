import React from 'react';

class Educations extends React.Component {
  render() {
    const { educations } = this.props;
    const listItem = educations.map(item => (
      <li key={educations.indexOf(item)}>
        <p className="year">{item.year}</p>
        <section>
          <p className="item-title">{item.title}</p>
          <p>{item.description}</p>
        </section>
      </li>
    ));
    return (
      <section>
        <h3>EDUCATION</h3>
        <ul className="list-of-education">{listItem}</ul>
      </section>
    );
  }
}
export default Educations;
