import React, { Component } from 'react';
import './App.less';
import Header from './component/Header';
import Description from './component/Description';
import Educations from './component/Educations';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      age: 0,
      description: '',
      educations: []
    };
    this.componentDidMount = this.componentDidMount.bind(this);
  }

  render() {
    return (
      <main className="app">
        <Header name={this.state.name} age={this.state.age} />
        <hr />
        <section>
          <Description description={this.state.description} />
          <Educations educations={this.state.educations} />
        </section>
      </main>
    );
  }

  componentDidMount() {
    fetch('http://localhost:3000/person')
      .then(response => response.json())
      .then(responseJson => {
        const { name, age, description, educations } = responseJson;
        this.setState({
          name,
          age,
          description,
          educations
        });
      })
      .catch(() => alert('Error'));
  }
}

export default App;
